﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Task02
{
    public struct Purchase
    {
        public int CreditCardID { get; set; }

        public string ShopName { get; set; }

        public decimal Cost { get; set; }

        public int PurchaseDate { get; set; }

        public void PrintPurchase()
        {
            Console.WriteLine($"cost:{Cost.ToString()};" +
                    $" cardID: {CreditCardID.ToString()};" +
                    $" purchase date: {PurchaseDate.ToString()};" +
                    $" shop: {ShopName}");
        }
    }
}
