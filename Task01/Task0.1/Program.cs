﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Task0._1.DataBank;
namespace Task0._1
{
    class Program
    {
        static void Main(string[] args)
        {
            string Path = args[0];
            string FileContent = DataKeeper.ReadFile(Path);
            Console.WriteLine("AllInfo");
            DataKeeper.WriteSting(FileContent);
            Console.WriteLine("All Nums");
            DataKeeper.WriteSting(DataKeeper.GetAllNumbers(FileContent));
            Console.WriteLine("Ordered Nums");
            DataKeeper.WriteSting(DataKeeper.OrderAllNumbers(FileContent));
            Console.WriteLine("AllSymbsOnce");
            DataKeeper.WriteSting(DataKeeper.WriteAllSymbolsOnce(FileContent));
            Console.ReadLine(); 
        }
    }
}
