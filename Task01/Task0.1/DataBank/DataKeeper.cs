﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;
namespace Task0._1.DataBank
{
    public class DataKeeper
    {
        /// <summary>
        /// get all content from the file
        /// </summary>
        /// <param name="fileName">filePath</param>
        /// <returns></returns>
        public static string ReadFile(string fileName)
        {
            string outputString;
            using (StreamReader strR = new StreamReader(fileName, Encoding.Default))
            {
                outputString = strR.ReadToEnd();
            }
            return outputString;
        }
        /// <summary>
        /// gets all numbers from file content
        /// </summary>
        /// <param name="fileContent"></param>
        /// <returns></returns>
        public static string GetAllNumbers(string fileContent)
        {
            string outputString = string.Empty;
            foreach (var a in fileContent.Where(t => t >= '0' && t <= '9'))
                outputString += a;
            return outputString;
        }

        public static string OrderAllNumbers(string stringOfNumbers)
        {
            string str = string.Empty;
            foreach (var a in stringOfNumbers.Select(t => t).Where(t => t >= '0' && t <= '9').OrderByDescending(t => t))
                str += a;
            return str;
        }
      
        public static string WriteAllSymbolsOnce(string fileContent)
        {
            string outputString = string.Empty;
            foreach (var a in fileContent)
                if (!outputString.Contains(a))
                    outputString += a;
            return outputString;
        }
       
        public static void WriteSting(string content)
        {
            Console.WriteLine(content);
        }
    }
}
