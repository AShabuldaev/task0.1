﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Task03
{
    public class Purchase
    {
        public int CreditCardID { get; set; }

        public string ShopName { get; set; }

        public decimal Cost { get; set; } 

        public int PurchaseDate { get; set; }

        public void PrintPurchase()
        {
            Console.WriteLine($"cost:{GetDiscountedCost().ToString()};" +
                    $" cardID: {CreditCardID.ToString()};" +
                    $" purchase date: {PurchaseDate.ToString()};" +
                    $" shop: {ShopName}" +
                    $"discount: {Discount * 100}%");
        }

        public decimal Discount { get; } = 0.05M;

        public decimal GetDiscountedCost()
        {
            return Cost * (1 - Discount);
        }
    }
}
